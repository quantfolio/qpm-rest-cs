﻿using System;
using System.Collections.Generic;
using System.Linq;
using QPM.Samples.Model;

namespace QPM.Samples.Program
{
    internal class Program
    {
        private static void Main()
        {
            var qpmService = new QPMService(new Uri(QPMConfiguration.Endpoint));

            // Calls the sign-in and sets up the authorization header
            qpmService.SignIn(QPMConfiguration.ApiKey);

            var input = new CustomerProfile
            {
                InvestmentHorizon = 4,
                RiskTolerance = 2,
                PeriodicSavings = 5000,
                StartingCapital = 100000,
                OptionList = new[] { "c10", "c105" },
            };

            // sets up params and calls the QPM enpoint
            var result = qpmService.GetPortfolioAdvice(input).Result;

            PrintResults(result);

            var portfolio = new Portfolio
            {
                Investments = new List<Investment> {
                    new Investment{
                        ISIN = "NO0010337579",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.5m
                    },
                    new Investment{
                        ISIN = "NO0010455694",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.25m
                    },
                    new Investment{
                        ISIN = "IE00B4L5Y983",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.25m
                    }
                }
            };

            var analyticsResults = qpmService.AnalysePortfolio(input, portfolio).Result;
            PrintResults(analyticsResults);
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }

        private static void PrintResults(AdviseResult result)
        {
            var medianForecast = GetTimeSeries(result.MarketReturns.Forecast, "Median").ToList();

            Console.WriteLine($"Number of forecast records returned: {medianForecast.Count}");
            Console.WriteLine($"First forecast date: {medianForecast.First().Item1}");
            Console.WriteLine($"Expected annual return: {result.ForecastedAnnualReturn}");
            Console.WriteLine();
            Console.WriteLine($"{"Weight",6} {"Class code",-10} {"Asset class",-35} {"ISIN",-12} {"PerformanceID",-12} {"ExpenseRatio",-12} {"Label",-20} ");
            foreach (var allocation in result.RecommendedPortfolio)
            {
                Console.WriteLine(
                    $"{allocation.Weight,6} {allocation.AssetClass.Code,-10} {allocation.AssetClass.Label,-35} {allocation.Instrument.ISIN,-12}  {allocation.Instrument.PerformanceId,-12}  {allocation.Instrument.ExpenseRatio,12} {allocation.Instrument.Name,-20} ");
            }
        }

        private static void PrintResults(PortfolioAnalyticsResult result)
        {
            Console.WriteLine();
            Console.WriteLine("What the robot-master thinks of your portfolio:");
            Console.WriteLine($"{"Advice",8} {"Class code",-10} {"Asset class",-35} {"ISIN",-12} {"Benchmark",-12}");

            foreach (var investmentAdvice in result.InvestmentAdvice)
            {
                Console.WriteLine(
                    $"{investmentAdvice.Advice,8} {investmentAdvice.AssetClass.Code,-10} {investmentAdvice.AssetClass.Label,-35} {investmentAdvice.Investment.ISIN,-12}  {investmentAdvice.Benchmark.Name,-12}");

            }

            Console.WriteLine();
            Console.WriteLine("How the robot-master thinks you should invest:");
            Console.WriteLine($"{"Weight",6} {"Class code",-10} {"Asset class",-35} {"ISIN",-12} {"PerformanceID",-12} {"ExpenseRatio",-12} {"Label",-20} ");
            foreach (var allocation in result.RecommendedPortfolio)
            {
                Console.WriteLine(
                    $"{allocation.Weight,6} {allocation.AssetClass.Code,-10} {allocation.AssetClass.Label,-35} {allocation.Instrument.ISIN,-12}  {allocation.Instrument.PerformanceId,-12}  {allocation.Instrument.ExpenseRatio,12} {allocation.Instrument.Name,-20} ");
            }
        }


        private static IEnumerable<Tuple<DateTime, decimal>> GetTimeSeries(Dictionary<DateTime, Dictionary<string, decimal>> series, string name)
        {
            return series
                .Where(s => s.Value.ContainsKey(name))
                .Select(s => new Tuple<DateTime, decimal>(s.Key, s.Value[name]));
        }
    }
}
