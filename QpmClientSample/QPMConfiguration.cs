﻿namespace QPM.Samples.Program
{
    public static class QPMConfiguration
    {
        public static string Endpoint { get; } = "ENDPOINT";
        public static string ApiKey { get; } = "APIKEY";
    }
}
