﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using QPM.Samples.Model;

namespace QPM.Samples.Program
{
    internal class QPMService
    {
        private readonly HttpClient _client;
        public QPMService(Uri endpoint)
        {
            _client = new HttpClient { BaseAddress = endpoint };

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void SignIn(string apiKey)
        {
            var response = _client.GetAsync("/api/SignIn?apiKey=" + apiKey).Result;
            response.EnsureSuccessStatusCode();
            var tokenResponse = response.Content.ReadAsAsync<AuthResponse>().Result;
            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenResponse.Token);
        }

        public async Task<AdviseResult> GetPortfolioAdvice(CustomerProfile input)
        {
            // As this is a GET Method parameters are passed via query string
            var query = System.Web.HttpUtility.ParseQueryString("");
            query["riskTolerance"] = input.RiskTolerance.ToString();
            query["investmentHorizon"] = input.InvestmentHorizon.ToString();
            query["periodicSavings"] = input.PeriodicSavings.ToString(CultureInfo.InvariantCulture);
            query["startingCapital"] = input.StartingCapital.ToString(CultureInfo.InvariantCulture);
            foreach (var option in input.OptionList)
            {
                query.Add("optionList", option);
            }

            var response = await _client.GetAsync("api/Qpm?" + query);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<AdviseResult>();
        }

        public async Task<PortfolioAnalyticsResult> AnalysePortfolio(CustomerProfile input, Portfolio portfolio)
        {
            var response = await _client.PostAsJsonAsync("api/Qpa", new { customerProfile = input, portfolio = portfolio });
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<PortfolioAnalyticsResult>();
        }
    }
}
