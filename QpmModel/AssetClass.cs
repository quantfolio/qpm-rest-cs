﻿namespace QPM.Samples.Model
{
    public class AssetClass
    {
        public string Code { get; set; }
        public string Label { get; set; }
    }
}