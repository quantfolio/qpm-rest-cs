﻿using System;
using System.Collections.Generic;

namespace QPM.Samples.Model
{
    public class SeriesPair {
        /// <summary>
        /// Historic performance of selected strategy
        /// </summary>
        public Dictionary<DateTime, Dictionary<string, decimal>> Backtest { get; set; }

        /// <summary>
        /// Simulated perfromance of selected strategy
        /// </summary>
        public Dictionary<DateTime, Dictionary<string, decimal>> Forecast { get; set; }
    }
}