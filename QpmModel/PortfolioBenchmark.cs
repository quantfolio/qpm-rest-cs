namespace QPM.Samples.Model
{
    public class PortfolioBenchmark { 
        public string Name { get; set; }
        public AnalysisData AnalysisData { get; set; }
    }
}