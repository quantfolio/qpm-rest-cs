using System.Collections.Generic;

namespace QPM.Samples.Model
{
    public class Portfolio
    {
        public IList<Investment> Investments { get; set; }
    }
}