namespace QPM.Samples.Model
{
    public class InvestmentAdvice
    {
        public PortfolioInstrument Investment { get; set; }
        public PortfolioBenchmark Benchmark { get; set; }
        public AdviceType Advice { get; set; }
        public AssetClass AssetClass { get; set; }
    }
}