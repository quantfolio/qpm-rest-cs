﻿namespace QPM.Samples.Model
{
    public class AuthResponse
    {
        public string Token { get; set; }
    }
}
