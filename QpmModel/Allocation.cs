﻿namespace QPM.Samples.Model
{
    /// <summary>
    /// Details on the allocation item, both the asset class - and the instrument constituning that asset class at the moment.
    /// </summary>
    public class Allocation {
        public AssetClass AssetClass { get; set; }
        public Instrument Instrument { get; set; }
        public decimal Weight { get; set; }
    }
}