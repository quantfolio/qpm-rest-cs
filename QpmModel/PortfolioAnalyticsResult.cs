using System.Collections.Generic;

namespace QPM.Samples.Model
{
    public class PortfolioAnalyticsResult
    {
        public IList<InvestmentAdvice> InvestmentAdvice { get; set; }
        public IList<Allocation> RecommendedPortfolio { get; set; }
    }
}
