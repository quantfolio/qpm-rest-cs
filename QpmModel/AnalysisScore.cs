namespace QPM.Samples.Model
{
    public class AnalysisScore
    {
        public double ReturnScore { get; set; }
        public double SharpeScore { get; set; }
    }
}