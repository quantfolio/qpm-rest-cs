namespace QPM.Samples.Model
{
    public class Investment
    {
        public string ISIN { get; set; }
        public string Currency { get; set; }
        public string Exchange { get; set; }
        public decimal Weight { get; set; } = -1.0m;
    }
}