﻿namespace QPM.Samples.Model
{
    public class Instrument
    {
        public string ISIN { get; set; }
        public decimal ExpenseRatio { get; set; }
        public string Name { get; set; }
        public string PerformanceId { get; set; }
    }
}