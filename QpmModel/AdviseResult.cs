﻿using System.Collections.Generic;

namespace QPM.Samples.Model
{
    /// <summary>
	/// Output of advice calculations, time series and recommended weights
	/// </summary>
    public class AdviseResult
    {
        /// <summary>
        /// Backtest and forecast, with periodic savings.
        /// </summary>
        public SeriesPair SavingsPlan { get; set; }

        /// <summary>
        /// Backtest and forecast, market returns normalized without periodic savings.
        /// </summary>
        public SeriesPair MarketReturns { get; set; }

        /// <summary>
        /// The recommended portfolio (set of allocations)
        /// </summary>
        public IList<Allocation> RecommendedPortfolio { get; set; }

        /// <summary>
        /// Expected average annual returns 
        /// </summary>
        public decimal ForecastedAnnualReturn { get; set; }
    }
}
