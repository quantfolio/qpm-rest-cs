namespace QPM.Samples.Model
{
    public class PortfolioInstrument
    {
        public string ISIN { get; set; }
        public string Name { get; set; }
        public AnalysisData AnalysisData { get; set; }
        public AnalysisScore AnalysisScore { get; set; }
    }
}