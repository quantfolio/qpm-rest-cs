﻿namespace QPM.Samples.Model
{
    /// <summary>
    /// Input to the qpm controller
    /// </summary>
    public class CustomerProfile
    {
        /// <summary>
        /// Integer between 1 (short) and 3 (long) representing the investment horizon.
        /// </summary>
        public int InvestmentHorizon { get; set; }

        /// <summary>
        /// Integer between 1 (low) and 4 (high) representing the willingness to trade risk for profits.
        /// </summary>
        public int RiskTolerance { get; set; }

        /// <summary>
        /// Amount of monthly savings to add to the portfolio
        /// </summary>
        public decimal PeriodicSavings { get; set; } 

        /// <summary>
        /// Initial amount for investment
        /// </summary>
        public decimal StartingCapital { get; set; } 

        /// <summary>
        /// Request for up to four optional classes. 
        /// </summary>
        public string[] OptionList { get; set; } = new string[0];
    }
}
