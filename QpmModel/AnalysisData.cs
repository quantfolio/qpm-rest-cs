namespace QPM.Samples.Model
{
    public class AnalysisData
    {
        public double YearsHistory { get; set; }
        public double Return1Y { get; set; }
        public double Return3Y { get; set; }
        public double Return5Y { get; set; }
        public double Return10Y { get; set; }
        public double ReturnFrontier { get; set; }
        public double Volatility1Y { get; set; }
        public double Volatility3Y { get; set; }
        public double Volatility5Y { get; set; }
        public double Volatility10Y { get; set; }
        public double VolatilityFrontier { get; set; }
    }
}