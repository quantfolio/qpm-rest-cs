﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using QPM.Samples.Model;
using QPM.Samples.Program;
using Xunit;

namespace QPM.Samples.Tests
{
    public class QpmTests
    {
        [Fact]
        public async void Qpm_ValidInput_ValidOutput()
        {
            var client = new HttpClient { BaseAddress = new Uri(QPMConfiguration.Endpoint) };
            var response = await client.GetAsync("/api/SignIn?apiKey=" + QPMConfiguration.ApiKey);
            response.EnsureSuccessStatusCode();
            var authResponse = await response.Content.ReadAsAsync<AuthResponse>();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authResponse.Token);

            var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
            query["riskTolerance"] = "1";
            query["investmentHorizon"] = "2";
            query["periodicSavings"] = "0";
            query["startingCapital"] = "1000";

            var qpmResponse = await client.GetAsync("api/Qpm?" + query);
            qpmResponse.EnsureSuccessStatusCode();
            var advise = await qpmResponse.Content.ReadAsAsync<AdviseResult>();
            Assert.NotNull(advise);
            Assert.NotNull(advise.ForecastedAnnualReturn);
            Assert.NotNull(advise.MarketReturns);
            Assert.NotNull(advise.RecommendedPortfolio);
            Assert.NotNull(advise.SavingsPlan);
            Assert.NotNull(advise.SavingsPlan.Backtest);
            Assert.NotNull(advise.SavingsPlan.Forecast);
            Assert.NotNull(advise.RecommendedPortfolio);
            Assert.Equal(advise.RecommendedPortfolio.GroupBy(x => x.Instrument).Count(), advise.RecommendedPortfolio.Count);
        }

        [Fact]
        public async void Qpm_MissingParmeter_400BadRequest()
        {
            var client = new HttpClient { BaseAddress = new Uri(QPMConfiguration.Endpoint) };
            var response = await client.GetAsync("/api/SignIn?apiKey=" + QPMConfiguration.ApiKey);
            response.EnsureSuccessStatusCode();
            var authResponse = await response.Content.ReadAsAsync<AuthResponse>();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authResponse.Token);

            var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
            query["investmentHorizon"] = "2";
            query["periodicSavings"] = "0";
            query["startingCapital"] = "1000";

            var qpmResponse = await client.GetAsync("api/Qpm?" + query);
            Assert.Equal(HttpStatusCode.BadRequest, qpmResponse.StatusCode);
        }

        [Fact]
        public async void SignIn_WrongKey_Unauthorized()
        {
            var client = new HttpClient {BaseAddress = new Uri(QPMConfiguration.Endpoint)};
            var response = await client.GetAsync("/api/SignIn?apiKey=nonsense");
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async void SignIn_NoKey_BadRequest()
        {
            var client = new HttpClient { BaseAddress = new Uri(QPMConfiguration.Endpoint) };
            var response = await client.GetAsync("/api/SignIn?apiKey=");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async void Qpm_NoAuthoriztionHeader_Unauthorized()
        {
            var client = new HttpClient { BaseAddress = new Uri(QPMConfiguration.Endpoint) };
            var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
            query["investmentHorizon"] = "2";
            query["periodicSavings"] = "0";
            query["startingCapital"] = "1000";

            var qpmResponse = await client.GetAsync("api/Qpm?" + query);
            Assert.Equal(HttpStatusCode.Unauthorized, qpmResponse.StatusCode);
        }
    }
}
