﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using QPM.Samples.Model;
using QPM.Samples.Program;
using Xunit;

namespace QPM.Samples.Tests
{
    public class QpaTests
    {
        [Fact]
        public async void Qpa_ValidInput_ValidOutput()
        {
            var client = new HttpClient { BaseAddress = new Uri(QPMConfiguration.Endpoint) };
            var response = await client.GetAsync("/api/SignIn?apiKey=" + QPMConfiguration.ApiKey);
            response.EnsureSuccessStatusCode();
            var authResponse = await response.Content.ReadAsAsync<AuthResponse>();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authResponse.Token);

            var customerProfile = new CustomerProfile
            {
                InvestmentHorizon = 4,
                RiskTolerance = 2,
                PeriodicSavings = 5000,
                StartingCapital = 100000,
                OptionList = new[] { "c10", "c105" },
            };

            // sets up params and calls the QPM enpoint

            var portfolio = new Portfolio
            {
                Investments = new List<Investment> {
                    new Investment{
                        ISIN = "NO0010337579",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.5m
                    },
                    new Investment{
                        ISIN = "NO0010455694",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.25m
                    },
                    new Investment{
                        ISIN = "IE00B4L5Y983",
                        Currency = "NOK",
                        Exchange = "",
                        Weight = 0.25m
                    }
                }
            };

            var qpmResponse = await client.PostAsJsonAsync("api/Qpa", new { customerProfile, portfolio });
            qpmResponse.EnsureSuccessStatusCode();
            var results = await qpmResponse.Content.ReadAsAsync<PortfolioAnalyticsResult>();
            Assert.NotNull(results);
            Assert.NotNull(results.InvestmentAdvice);
            Assert.NotNull(results.RecommendedPortfolio);
            Assert.NotNull(results.InvestmentAdvice.Count == 3);
            Assert.NotNull(results.RecommendedPortfolio.Count > 0);
        }
    }
}
