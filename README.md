# QPM Sample C# client #

This is simple console application fetches a token and calls the calculate.

### How do I get set up? ###

* Download and open this repository in Visual Studio
* Install Nuget packages, if necessary
* In QPMConfiguration replace ENDPOINT and APIKEY with your respective values.  (This is used both in console program and for integration tests.)
* Run QPMSampleClient project or integration tests.